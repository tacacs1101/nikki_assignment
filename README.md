# Niki.ai Assignment #

This README would normally document whatever steps are necessary to get your application up and running.

### Text classification ###

This repository experiments with the various ML techniques used for tex classification on data set given as the part assignment by niki.ai. The data set consists of only 1483 samples and 5 labels.
The training sample consists of a sentence (Questionarrie ) and labels associated with it.

example:- sample: 'who did this ?', label: who .
The data set was quite small from nlp perspective and all models  except word embedding model suffer from overfitting. However i tried my best to give a try to most successful model for text
classification and tuned its parameter using cross validation strategy. Once the full version of data set is provided, it will produce excellent results. I tried several techniques from classical ML to deep learning models. The code
are written using scikit-learn and keras library in python.

### How do I get set up? ###
The code is written in jupyter notebook and .ipynb file is attached to repository. For installing all of dependencies, please install Anaconda.
After installing Anaconda, please install keras using command: 

** conda install keras **

For executing the code, open the .ipynb file using jupyter notebook and run cell one after another. I highly recommend to execute cell in sequence. This will prevent inconsistencies. You can view my whole jupyter notebook as pdf
or html. pdf looks slightly awakward, so please download the html file and view results in your browser. The jupyter notebook is well formatted with results.

### ML model used for text classification ###

#### SVM ####
SVM is consider good for text classification as text data are high dimensional and svm is quite good at classifying it.

**stps involved**

1. ***Text preprocessing:** It involve setence tokenization, Lemmatization,puntuation removal.

2. *** Vectorization:*** It involve using tf-idf technique to vectorize the sentence.

3. *** Dimensionality reduction:*** It used Latent semantic analysis to reduce the dimensionality of sparse tf-idf matrix.

4. *** Modeling:*** Using support vector machine with linear kernels to train the classifier.

5. *** Hyper-parameter tunning:*** Tuned the parameters involved in each step using cross validation strategy.

*** Performance: *** Models performs good on training data but perform poorly on test data. Due to less amount of data, it just memorize the training data.

#### Multinomial Naive Baiyes ####

Multinomials models are considered good for text classification when features are independent.

1. *** Text preprocessing:*** same as above.

2. *** Vectorization: *** Using count of words in each document to vectorize the sentences.

3. *** Modelling: *** Using multinomial naive bayes to train the model.

*** Performance:*** Not good performance as features are not independent.

#### Logistic Regression ####

1. *** Text preprocessing: *** Same as above

2. *** Vectorization: *** Using tf-idf technique.

3. *** Modelling: *** Using the regularized logistic regression.

4. *** Hyper-parameter tunning: *** trained various parameters especially regularization , using cross validation strategy.  

*** Performance: *** Surprisingly outperforms the powerful model like SVM on test accuracy. This illustrates the power of linear model for small data sets.

#### Word Embedding Model ####

1. *** Text preprocessing:*** Same as above.

2. *** Vectorization:*** Using pretrained glove model to vectorize each words into 100 dimensional vectors.

3. *** Modelling:*** Using simple neural network with just 1 hiddne layer with 5 units for classification.

*** Performance: *** Excellent performance on train and test data set. slightly low performance on test than train data.




*** Author:*** Anand Rahul
